```sql
CREATE TABLE `mpass`.`sys_org_category` (
`fd_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`fd_tenant_id` int(11) NULL DEFAULT NULL,
`fd_type` int(11) NOT NULL,
`fd_hierarchy_id` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_tree_level` int(11) NULL DEFAULT NULL,
`fd_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_order` int(11) NULL DEFAULT NULL,
`fd_create_time` datetime NULL DEFAULT NULL,
`fd_alter_time` datetime NULL DEFAULT NULL,
`fd_last_modified_time` datetime NULL DEFAULT NULL,
`fd_parent_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
PRIMARY KEY (`fd_id`) ,
CONSTRAINT `sys_org_category_ibfk_1` FOREIGN KEY (`fd_parent_id`) REFERENCES `mpass`.`sys_org_category` (`fd_id`),
INDEX `IDX983finqsvie6m3l3k104uy1ov` (`fd_hierarchy_id`),
INDEX `IDX77f40rxnvt05j82hor7ir38v0` (`fd_create_time`),
INDEX `IDXpqbiv3g4gncobkj9osmco2set` (`fd_last_modified_time`),
INDEX `FKpnwvvt4oo6aadxvuesirof7xp` (`fd_parent_id`)
);


CREATE TABLE `mpass`.`sys_org_element` (
`fd_org_type` int(11) NOT NULL,
`fd_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`fd_tenant_id` int(11) NULL DEFAULT NULL,
`fd_describe` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_hierarchy_id` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_import_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_import_provider` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_is_available` bit(1) NOT NULL DEFAULT 0,
`fd_is_business` bit(1) NOT NULL DEFAULT 0,
`fd_name_pinyin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_name_simple_pinyin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_persons_number` int(11) NULL DEFAULT NULL,
`fd_work_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_person_type` int(11) NULL DEFAULT NULL,
`fd_tree_level` int(11) NULL DEFAULT NULL,
`fd_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_order` int(11) NULL DEFAULT NULL,
`fd_create_time` datetime NULL DEFAULT NULL,
`fd_alter_time` datetime NULL DEFAULT NULL,
`fd_last_modified_time` datetime NULL DEFAULT NULL,
`fd_name_us` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_name_cn` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_name_hk` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_ori_parent_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_parent_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_parent_org_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_this_leader_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_category_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_super_leader_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_account_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_staffing_level_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_ori_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_creator_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
PRIMARY KEY (`fd_id`) ,
CONSTRAINT `sys_org_element_ibfk_2` FOREIGN KEY (`fd_ori_parent_id`) REFERENCES `mpass`.`sys_org_element` (`fd_id`),
CONSTRAINT `sys_org_element_ibfk_3` FOREIGN KEY (`fd_super_leader_id`) REFERENCES `mpass`.`sys_org_element` (`fd_id`),
CONSTRAINT `sys_org_element_ibfk_4` FOREIGN KEY (`fd_parent_org_id`) REFERENCES `mpass`.`sys_org_element` (`fd_id`),
CONSTRAINT `sys_org_element_ibfk_5` FOREIGN KEY (`fd_category_id`) REFERENCES `mpass`.`sys_org_category` (`fd_id`),
CONSTRAINT `sys_org_element_ibfk_6` FOREIGN KEY (`fd_parent_id`) REFERENCES `mpass`.`sys_org_element` (`fd_id`),
CONSTRAINT `sys_org_element_ibfk_7` FOREIGN KEY (`fd_this_leader_id`) REFERENCES `mpass`.`sys_org_element` (`fd_id`),
INDEX `IDX4648ibhnq8k61pciaqg5wptmm` (`fd_hierarchy_id`),
INDEX `IDX90eht8efet19a0qab7umjlkqb` (`fd_create_time`),
INDEX `IDXkhhf801im354yjt2m8dy8u6v` (`fd_last_modified_time`),
INDEX `FK2ycdkuwdmmbaxarj2air39jeq` (`fd_ori_parent_id`),
INDEX `FKsjyk2sv8lpea6l4tbgvslb96j` (`fd_parent_id`),
INDEX `FKoskm6bm9wbk0yjpiin4ltwyu3` (`fd_parent_org_id`),
INDEX `FKss0b9fq2qeqameafp6is0k4lh` (`fd_this_leader_id`),
INDEX `FKpjvtfyaln24daby721yq9djn5` (`fd_category_id`),
INDEX `FKcxwxp7y6mclb6ejo6lrgp6alk` (`fd_super_leader_id`),
INDEX `FK239pof1ak32bdt8djxgbkkval` (`fd_account_id`),
INDEX `FKr6hxtg0yup5yuoxtpjw6dsx6r` (`fd_staffing_level_id`),
INDEX `IDX1x0eyl1en5udlj7ylv2dutr5g` (`fd_ori_id`)
);


CREATE TABLE `mpass`.`sys_org_element_admin` (
`fd_element_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`fd_admin_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
CONSTRAINT `sys_org_element_admin_ibfk_1` FOREIGN KEY (`fd_element_id`) REFERENCES `mpass`.`sys_org_element` (`fd_id`),
CONSTRAINT `sys_org_element_admin_ibfk_2` FOREIGN KEY (`fd_admin_id`) REFERENCES `mpass`.`sys_org_element` (`fd_id`),
INDEX `FK6pgg23ayl6rp44o0sqjsg84h7` (`fd_admin_id`),
INDEX `FK1ya0g1bc51ghvxfrswx8x3fkd` (`fd_element_id`)
);


CREATE TABLE `mpass`.`sys_org_element_summary` (
`fd_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`fd_tenant_id` int(11) NULL DEFAULT NULL,
`fd_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_is_available` bit(1) NULL DEFAULT NULL,
`fd_login_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_org_type` int(11) NULL DEFAULT NULL,
`fd_ori_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_hierarchy_id` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_parent_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_tree_level` int(11) NULL DEFAULT NULL,
`fd_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_last_modified_time` datetime NULL DEFAULT NULL,
`fd_name_us` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_name_cn` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_parent_org_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_name_hk` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
PRIMARY KEY (`fd_id`) ,
INDEX `IDXpbsxj7blqdposus93vyif93o0` (`fd_hierarchy_id`),
INDEX `IDXjuuu4b7po7ppnwfa88c2u34qf` (`fd_last_modified_time`),
INDEX `IDXem28kay29xn2y1ausuxpdodvu` (`fd_login_name`),
INDEX `IDXsmku6ig29pmcu4vymb61jk2g` (`fd_ori_id`)
);


CREATE TABLE `mpass`.`sys_org_group_element` (
`fd_group_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`fd_element_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
CONSTRAINT `sys_org_group_element_ibfk_1` FOREIGN KEY (`fd_element_id`) REFERENCES `mpass`.`sys_org_element` (`fd_id`),
CONSTRAINT `sys_org_group_element_ibfk_2` FOREIGN KEY (`fd_group_id`) REFERENCES `mpass`.`sys_org_element` (`fd_id`),
INDEX `FKp8yk7gpmxpm735spgeac7rp3y` (`fd_element_id`),
INDEX `FKqrr1jv4879lap66rmjt36tb6i` (`fd_group_id`)
);



```